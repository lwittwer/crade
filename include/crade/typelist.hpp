//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_TYPELIST_HPP
#define CRADE_TYPELIST_HPP

#include <tuple>
#include <type_traits>

namespace crade {

template <class... Types>
struct typelist {};

template <class Typelist>
struct typelist_size;

template <class... Types>
struct typelist_size<typelist<Types...>>
    : std::integral_constant<size_t, sizeof...(Types)> {};

template <size_t I, class Typelist>
struct typelist_at;

template <size_t I, class Head, class... Tail>
struct typelist_at<I, typelist<Head, Tail...>>
    : typelist_at<I - 1, typelist<Tail...>> {};

template <class Head, class... Tail>
struct typelist_at<0, typelist<Head, Tail...>> {
  typedef Head type;
};

template <class LeftTypelist, class RightTypelist>
struct typelist_cat;

template <class... LeftTypes, class... RightTypes>
struct typelist_cat<typelist<LeftTypes...>, typelist<RightTypes...>> {
  typedef typelist<LeftTypes..., RightTypes...> type;
};

template <size_t I, class Typelist, class T>
struct typelist_set;

template <size_t I, class T, class Head, class... Tail>
struct typelist_set<I, typelist<Head, Tail...>, T> {
  typedef typename typelist_cat<
      typelist<Head>,
      typename typelist_set<I - 1, typelist<Tail...>, T>::type>::type type;
};

template <class T, class Head, class... Tail>
struct typelist_set<0, typelist<Head, Tail...>, T> {
  typedef typelist<T, Tail...> type;
};

template <class Tuple>
struct typelist_from_tuple;

template <class... Types>
struct typelist_from_tuple<std::tuple<Types...>> {
  typedef typelist<Types...> type;
};

template <class Typelist>
struct typelist_to_tuple;

template <class... Types>
struct typelist_to_tuple<typelist<Types...>> {
  typedef std::tuple<Types...> type;
};

template <class T, size_t I>
struct typelist_uniform {
  typedef typename typelist_cat<
      typelist<T>, typename typelist_uniform<T, I - 1>::type>::type type;
};

template <class T>
struct typelist_uniform<T, 0> {
  typedef typelist<> type;
};

template <class Typelist, class Inverted = typelist<>>
struct typelist_inverted;

template <class Head, class... Tail, class... Inverted>
struct typelist_inverted<typelist<Head, Tail...>, typelist<Inverted...>> {
  typedef typelist_inverted<typelist<Tail...>,
                            typelist<Head, Inverted...>>::type type;
};

template <class Inverted>
struct typelist_inverted<typelist<>, Inverted> {
  typedef Inverted type;
}
}

#endif  // CRADE_TYPELIST_HPP

//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_SORTERS_HPP
#define CRADE_SORTERS_HPP

#include <algorithm>
#include <array>

#include "crade/prefix_sum.hpp"
#include "crade/traits.hpp"

namespace crade {

namespace detail {

template <class BucketStorage, class... DigitExtractors>
struct serial_lsb_radix_sorter;

template <class BucketStorage, class Digit, class... OtherDigits>
struct serial_lsb_radix_sorter<BucketStorage, Digit, OtherDigits...> {
  template <class RandomIt>
  static void sort(RandomIt first, RandomIt last, Digit digit,
                   OtherDigits... other_digits) {
    typedef typename storage_traits<
        BucketStorage>::template rebind_bucket_count<Digit::digit_states>
        digit_storage;

    digit_storage buckets(std::distance(first, last));

    for (RandomIt i = first; i != last; ++i) {
      const auto item = *i;
      buckets[digit(item)].push_back(item);
    }
    buckets.flush();

    serial_lsb_radix_sorter<digit_storage, OtherDigits...>::sort_recursion(
        buckets, first, other_digits...);
  }

  template <class RandomIt>
  static void sort_recursion(BucketStorage& src, RandomIt first, Digit digit,
                             OtherDigits... other_digits) {
    typedef typename storage_traits<
        BucketStorage>::template rebind_bucket_count<Digit::digit_states>
        digit_storage;

    digit_storage dst(src.bucket_size());

    for (auto bucket : src)
      for (auto item : bucket) dst[digit(item)].push_back(item);
    dst.flush();

    serial_lsb_radix_sorter<digit_storage, OtherDigits...>::sort_recursion(
        dst, first, other_digits...);
  }
};

template <class BucketStorage, class Digit1, class Digit2>
struct serial_lsb_radix_sorter<BucketStorage, Digit1, Digit2> {
  template <class RandomIt>
  static void sort(RandomIt first, RandomIt last, Digit1 digit1,
                   Digit2 digit2) {
    typedef typename storage_traits<
        BucketStorage>::template rebind_bucket_count<Digit1::digit_states>
        digit1_storage;

    digit1_storage buckets(std::distance(first, last));

    std::array<std::size_t, Digit2::digit_states> histogram;
    std::fill(histogram.begin(), histogram.end(), 0);

    for (RandomIt i = first; i != last; ++i) {
      const auto item = *i;
      buckets[digit1(item)].push_back(item);
      ++histogram[digit2(item)];
    }
    buckets.flush();

    exclusive_prefix_sum(histogram.begin(), histogram.end());

    for (auto bucket : buckets)
      for (auto item : bucket) {
        const auto d = digit2(item);
        first[histogram[d]] = item;
        ++histogram[d];
      }
  }

  template <class RandomIt>
  static void sort_recursion(BucketStorage& src, RandomIt first, Digit1 digit1,
                             Digit2 digit2) {
    typedef typename storage_traits<
        BucketStorage>::template rebind_bucket_count<Digit1::digit_states>
        digit1_storage;

    digit1_storage dst(src.bucket_size());

    std::array<std::size_t, Digit2::digit_states> histogram;
    std::fill(histogram.begin(), histogram.end(), 0);

    for (auto bucket : src)
      for (auto item : bucket) {
        dst[digit1(item)].push_back(item);
        ++histogram[digit2(item)];
      }
    dst.flush();

    exclusive_prefix_sum(histogram.begin(), histogram.end());

    for (auto bucket : dst)
      for (auto item : bucket) {
        const auto d = digit2(item);
        first[histogram[d]] = item;
        ++histogram[d];
      }
  }
};

template <class BucketStorage, class... DigitExtractors>
struct serial_msb_radix_sorter;

template <class BucketStorage, class Digit, class... OtherDigits>
struct serial_msb_radix_sorter<BucketStorage, Digit, OtherDigits...> {
  template <class RandomIt>
  static void sort(RandomIt first, RandomIt last, Digit digit,
                   OtherDigits... other_digits) {
    sort_recursion(first, last, first, digit, other_digits...);
  }

  template <class ForwardIt, class RandomIt>
  static void sort_recursion(ForwardIt first, ForwardIt last, RandomIt d_first,
                             Digit digit, OtherDigits... other_digits) {
    if (first == last) return;

    typedef typename storage_traits<
        BucketStorage>::template rebind_bucket_count<Digit::digit_states>
        digit_storage;

    digit_storage buckets(std::distance(first, last));

    for (ForwardIt i = first; i != last; ++i) {
      const auto item = *i;
      buckets[digit(item)].push_back(item);
    }
    buckets.flush();

    std::array<std::size_t, Digit::digit_states> histogram;
    for (std::size_t i = 0; i < Digit::digit_states; ++i)
      histogram[i] = buckets[i].size();
    exclusive_prefix_sum(histogram.begin(), histogram.end());

    for (std::size_t i = 0; i < Digit::digit_states; ++i) {
      auto bucket = buckets[i];
      auto out = d_first + histogram[i];
      serial_msb_radix_sorter<BucketStorage, OtherDigits...>::sort_recursion(
          bucket.begin(), bucket.end(), out, other_digits...);
    }
  }
};

template <class BucketStorage, class Digit>
struct serial_msb_radix_sorter<BucketStorage, Digit> {
  template <class ForwardIt, class RandomIt>
  static void sort_recursion(ForwardIt first, ForwardIt last, RandomIt d_first,
                             Digit digit) {
    if (first == last) return;

    std::array<std::size_t, Digit::digit_states> histogram;
    std::fill(histogram.begin(), histogram.end(), 0);

    for (ForwardIt i = first; i != last; ++i) {
      const auto item = *i;
      ++histogram[digit(item)];
    }

    exclusive_prefix_sum(histogram.begin(), histogram.end());
    for (ForwardIt i = first; i != last; ++i) {
      const auto item = *i;
      const auto d = digit(item);
      d_first[histogram[d]] = item;
      ++histogram[d];
    }
  }
};

template <class DigitExtractor>
struct multihist_traits {
  typedef std::array<std::size_t, DigitExtractor::digit_states> histogram_type;
};

template <class T, class... DigitExtractors>
class multihist_radix_sorter_helper;

template <class T>
class multihist_radix_sorter_helper<T> {
 public:
  multihist_radix_sorter_helper(std::size_t n) : data_(n) {}

  void update_histogram(const T&) {}
  void sum() {}

  std::vector<T>& data() { return data_; }

 private:
  std::vector<T> data_;
};

template <class T, class Digit, class... OtherDigits>
class multihist_radix_sorter_helper<T, Digit, OtherDigits...>
    : public multihist_radix_sorter_helper<T, OtherDigits...> {
 public:
  typedef multihist_radix_sorter_helper<T, OtherDigits...> parent;
  typedef typename multihist_traits<Digit>::histogram_type histogram_type;

  multihist_radix_sorter_helper(std::size_t n, Digit d, OtherDigits... ds)
      : parent(n, ds...) {
    std::fill(histogram_.begin(), histogram_.end(), 0);
  }

  void update_histogram(const T& value) {
    ++histogram_[digit_(value)];
    parent::update_histogram(value);
  }

  void sum() {
    exclusive_prefix_sum(histogram_.begin(), histogram_.end());
    parent::sum();
  }

  histogram_type& histogram() { return histogram_; }

 private:
  Digit digit_;
  histogram_type histogram_;
};

template <class DigitExtractor, class MultihistRadixSorterHelper>
struct histogram_getter;

template <class WantedDigit, class T, class Digit, class... OtherDigits>
struct histogram_getter<
    WantedDigit, multihist_radix_sorter_helper<T, Digit, OtherDigits...>> {
  static typename multihist_traits<WantedDigit>::histogram_type& get(
      multihist_radix_sorter_helper<T, Digit, OtherDigits...>& helper) {
    return histogram_getter<WantedDigit, multihist_radix_sorter_helper<
                                             T, OtherDigits...>>::get(helper);
  }
};

template <class WantedDigit, class T, class... OtherDigits>
struct histogram_getter<WantedDigit, multihist_radix_sorter_helper<
                                         T, WantedDigit, OtherDigits...>> {
  static typename multihist_traits<WantedDigit>::histogram_type& get(
      multihist_radix_sorter_helper<T, WantedDigit, OtherDigits...>& helper) {
    return helper.histogram();
  }
};

template <class Digit, class Helper>
typename multihist_traits<Digit>::histogram_type& get_histogram(
    Helper& helper) {
  return histogram_getter<Digit, Helper>::get(helper);
}

template <class... DigitExtractors>
struct multihist_radix_sorter;

template <class Digit, class... OtherDigits>
struct multihist_radix_sorter<Digit, OtherDigits...> {
  template <class RandomIt>
  static void sort(RandomIt first, RandomIt last, Digit digit,
                   OtherDigits... other_digits) {
    typedef typename std::iterator_traits<RandomIt>::value_type value_type;

    const auto n = std::distance(first, last);

    multihist_radix_sorter_helper<value_type, Digit, OtherDigits...> helper(
        n, digit, other_digits...);

    for (RandomIt i = first; i != last; ++i) helper.update_histogram(*i);

    helper.sum();

    sort_recursion(first, last, helper, false, digit, other_digits...);
  }

  template <class RandomIt, class Helper>
  static void sort_recursion(RandomIt first, RandomIt last, Helper& helper,
                             bool inverse, Digit digit,
                             OtherDigits... other_digits) {
    auto& datatmp = helper.data();
    auto& histogram = get_histogram<Digit>(helper);
    if (inverse) {
      for (auto i = datatmp.cbegin(); i != datatmp.cend(); ++i) {
        const auto item = *i;
        const auto j = digit(item);
        first[histogram[j]++] = item;
      }
    } else {
      for (RandomIt i = first; i != last; ++i) {
        const auto item = *i;
        const auto j = digit(item);
        datatmp[histogram[j]++] = item;
      }
    }

    multihist_radix_sorter<OtherDigits...>::sort_recursion(
        first, last, helper, !inverse, other_digits...);
  }
};

template <>
struct multihist_radix_sorter<> {
  template <class RandomIt, class Helper>
  static void sort_recursion(RandomIt first, RandomIt last, Helper& helper,
                             bool inverse) {
    if (inverse) {
      auto& datatmp = helper.data();
      std::copy(datatmp.begin(), datatmp.end(), first);
    }
  }
};
}
}

#endif  // CRADE_SORTERS_HPP

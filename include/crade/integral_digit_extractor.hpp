//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_INTEGRAL_DIGIT_EXTRACTOR_HPP
#define CRADE_INTEGRAL_DIGIT_EXTRACTOR_HPP

#include <cassert>
#include <climits>
#include <type_traits>

namespace crade {

template <class T, std::size_t D, std::size_t B = CHAR_BIT,
          std::size_t S = D* B, bool U = std::is_unsigned<T>::value>
class integral_digit_extractor;

template <class T, std::size_t D, std::size_t B, std::size_t S>
class integral_digit_extractor<T, D, B, S, true> {
 public:
  typedef T value_type;
  typedef std::size_t size_type;
  typedef std::size_t digit_type;
  static constexpr std::size_t digit_states = 1 << B;
  static constexpr std::size_t digit_number = D;

 public:
  constexpr digit_type operator()(const T& value) const {
    return (value >> S) & ((1 << B) - 1);
  }

  constexpr size_type digit() const { return D; }
};

template <class T, std::size_t D, std::size_t B, std::size_t S>
class integral_digit_extractor<T, D, B, S, false> {
 public:
  typedef T value_type;
  typedef std::size_t size_type;
  typedef std::size_t digit_type;
  static constexpr std::size_t digit_states = 1 << B;
  static constexpr std::size_t digit_number = D;

 private:
  static constexpr value_type sign_mask =
      value_type(1) << ((sizeof(value_type) * CHAR_BIT) - 1);

 public:
  constexpr digit_type operator()(const T& value) const {
    return ((sign_mask ^ value) >> S) & ((1 << B) - 1);
  }

  constexpr size_type digit() const { return D; }
};

}  // crade

#endif  // CRADE_INTEGRAL_DIGIT_EXTRACTOR_HPP

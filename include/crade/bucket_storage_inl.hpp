//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_BUCKET_STORAGE_HPP
#error "invalid include"
#endif

namespace crade {

template <class T, std::size_t N, class Allocator>
bucket_storage<T, N, Allocator>::bucket_storage(std::size_t bucket_size,
                                                const Allocator& alloc)
    : bucket_size_(bucket_size), alloc_(alloc) {
  allocate_storage();
  clear();
}

template <class T, std::size_t N, class Allocator>
bucket_storage<T, N, Allocator>::~bucket_storage() {
  free_storage();
}

template <class T, std::size_t N, class Allocator>
void bucket_storage<T, N, Allocator>::clear() {
  for (std::size_t i = 0; i < N; ++i) next_[i] = storage_ + i * bucket_size_;
}

template <class T, std::size_t N, class Allocator>
void bucket_storage<T, N, Allocator>::flush() {}

template <class T, std::size_t N, class Allocator>
inline void bucket_storage<T, N, Allocator>::add(std::size_t bucket,
                                                 const T& value) {
  assert(bucket < N);
  *next_[bucket] = value;
  ++next_[bucket];
}

template <class T, std::size_t N, class Allocator>
inline const T* bucket_storage<T, N, Allocator>::bucket_begin(
    std::size_t bucket) const {
  assert(bucket < N);
  return &storage_[bucket * bucket_size_];
}

template <class T, std::size_t N, class Allocator>
inline const T* bucket_storage<T, N, Allocator>::bucket_end(
    std::size_t bucket) const {
  assert(bucket < N);
  return next_[bucket];
}

template <class T, std::size_t N, class Allocator>
bucket<bucket_storage<T, N, Allocator> >
bucket_storage<T, N, Allocator>::bucket(std::size_t index) {
  return bucket<bucket_storage<T, N, Allocator> >(*this, index);
}

template <class T, std::size_t N, class Allocator>
Allocator bucket_storage<T, N, Allocator>::get_allocator() const {
  return alloc_;
}

}  // crade

//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_BUCKET_ITERATOR_HPP
#define CRADE_BUCKET_ITERATOR_HPP

#include <iterator>

#include "crade/bucket.hpp"

namespace crade {

template <class BucketStorage>
class bucket_iterator {
 public:
  typedef typename BucketStorage::size_type size_type;
  typedef bucket<BucketStorage> value_type;
  typedef bucket<BucketStorage>& reference;
  typedef const bucket<BucketStorage>& const_reference;
  typedef std::input_iterator_tag iterator_category;

 public:
  bucket_iterator(BucketStorage& buckets, size_type index)
      : current_bucket_(buckets, index) {}

  bucket<BucketStorage>& operator*() { return current_bucket_; }
  const bucket<BucketStorage>& operator*() const { return current_bucket_; }

  bucket<BucketStorage>* operator->() { return &current_bucket_; }

  bucket_iterator& operator++() {
    ++current_bucket_.index();
    return *this;
  }
  bucket_iterator operator++(int) {
    bucket_iterator i(*this);
    ++*this;
    return i;
  }

  bool operator==(const bucket_iterator& other) const {
    return current_bucket_ == other.current_bucket_;
  }

  bool operator!=(const bucket_iterator& other) const {
    return current_bucket_ != other.current_bucket_;
  }

 private:
  bucket<BucketStorage> current_bucket_;
};

}  // crade

#endif  // CRADE_BUCKET_ITERATOR_HPP

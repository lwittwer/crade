//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_BUCKET_STORAGE_HPP
#define CRADE_BUCKET_STORAGE_HPP

#include <array>
#include <cassert>

#include "crade/allocator.hpp"
#include "crade/bucket.hpp"
#include "crade/bucket_iterator.hpp"

namespace crade {

template <class T, std::size_t N, class Allocator = default_huge_allocator<T> >
class bucket_storage {
 public:
  typedef std::size_t size_type;
  typedef bucket<bucket_storage> value_type;
  typedef bucket_iterator<bucket_storage> iterator;

  typedef T bucket_value_type;
  typedef const T* bucket_value_iterator;
  typedef Allocator alloc_type;

  static constexpr std::size_t bucket_count = N;

  template <std::size_t N2>
  struct rebind_bucket_count {
    typedef bucket_storage<T, N2, Allocator> other;
  };

 public:
  bucket_storage(std::size_t bucket_size, const Allocator& alloc = Allocator())
      : bucket_size_(bucket_size), alloc_(alloc) {
    storage_ = alloc_.allocate(N * bucket_size_);
    clear();
  }

  bucket_storage(const bucket_storage&) = delete;

  bucket_storage(bucket_storage&& other)
      : bucket_size_(other.bucket_size_),
        storage_(other.storage_),
        next_(other.next_),
        alloc_(other.alloc_) {
    other.storage_ = nullptr;
  }

  ~bucket_storage() {
    if (storage_ != nullptr) {
      alloc_.deallocate(storage_, N * bucket_size_);
      storage_ = nullptr;
    }
  }

  void clear();

  void flush(){};

  void push_back(std::size_t index, const T& value) noexcept {
    assert(index < N);
    *next_[index] = value;
    ++next_[index];
  }

  const T* bucket_begin(std::size_t index) const {
    assert(index < N);
    return &storage_[index * bucket_size_];
  }

  const T* bucket_end(std::size_t index) const {
    assert(index < N);
    return next_[index];
  }

  bucket<bucket_storage> operator[](std::size_t index) {
    assert(index < N);
    return bucket<bucket_storage>(*this, index);
  }

  bucket_iterator<bucket_storage> begin() {
    return bucket_iterator<bucket_storage>(*this, 0);
  }

  bucket_iterator<bucket_storage> end() {
    return bucket_iterator<bucket_storage>(*this, N);
  }

  Allocator get_allocator() const { return alloc_; }

  std::size_t bucket_size() const { return bucket_size_; }

 private:
  const std::size_t bucket_size_;
  T* storage_;
  std::array<T*, N> next_;
  Allocator alloc_;
};

template <class T, std::size_t N, class Allocator>
void bucket_storage<T, N, Allocator>::clear() {
  for (std::size_t i = 0; i < N; ++i) next_[i] = storage_ + i * bucket_size_;
}

}  // crade

#endif  // CRADE_BUCKET_STORAGE_HPP

#include <algorithm>
#include <chrono>
#include <functional>
#include <iostream>
#include <random>

#include "boost/timer/timer.hpp"

#include "crade/bucket_storage.hpp"
#include "crade/buffered_bucket_storage.hpp"
#include "crade/sorters.hpp"

#include "int_sort.hpp"

class pair {
 public:
  int first;
  int second;

  bool operator<(const pair &other) const { return first < other.first; }
};

template <std::size_t D, std::size_t B = CHAR_BIT, std::size_t S = D *B>
class pair_digit_extractor
    : public crade::integral_digit_extractor<int, D, B, S> {
 public:
  typedef typename crade::integral_digit_extractor<int, D, B, S> parent;
  typedef pair value_type;
  typedef typename parent::size_type size_type;
  typedef typename parent::digit_type digit_type;

 public:
  constexpr digit_type operator()(const pair &value) const {
    return parent::operator()(value.first);
  }
};

int main(int argc, const char **argv) {
  std::mt19937 gen(std::chrono::system_clock::now().time_since_epoch().count());
  std::uniform_int_distribution<int> dist(std::numeric_limits<int>::min(),
                                          std::numeric_limits<int>::max());
  // std::uniform_int_distribution<int> dist;
  auto rand = std::bind(dist, std::ref(gen));

  const std::size_t n = argc > 1 ? std::stoi(argv[1]) : 1000000;

  {
    pair *data = new pair[n];
    assert(data);

    std::generate_n(reinterpret_cast<int *>(data), 2 * n, rand);
    std::cout << "std::sort (pair)" << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      std::sort(data, data + n);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    pair *data = new pair[n];
    assert(data);

    std::generate_n(reinterpret_cast<int *>(data), 2 * n, rand);
    std::cout << "crade::detail::serial_lsb_radix_sorter::sort (pair)"
              << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef pair_digit_extractor<0> d0;
      typedef pair_digit_extractor<1> d1;
      typedef pair_digit_extractor<2> d2;
      typedef pair_digit_extractor<3> d3;
      typedef crade::buffered_bucket_storage<pair, d0::digit_states, 64>
          storage;

      d0 di0;
      d1 di1;
      d2 di2;
      d3 di3;
      crade::detail::serial_lsb_radix_sorter<storage, d0, d1, d2, d3>::sort(
          data, data + n, di0, di1, di2, di3);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    pair *data = new pair[n];
    assert(data);

    std::generate_n(reinterpret_cast<int *>(data), 2 * n, rand);
    std::cout << "crade::detail::multihist_radix_sorter (pair, 8bit radix)"
              << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef pair_digit_extractor<0> d0;
      typedef pair_digit_extractor<1> d1;
      typedef pair_digit_extractor<2> d2;
      typedef pair_digit_extractor<3> d3;

      d0 di0;
      d1 di1;
      d2 di2;
      d3 di3;
      crade::detail::multihist_radix_sorter<d0, d1, d2, d3>::sort(
          data, data + n, di0, di1, di2, di3);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    pair *data = new pair[n];
    assert(data);

    std::generate_n(reinterpret_cast<int *>(data), 2 * n, rand);
    std::cout << "crade::detail::multihist_radix_sorter (pair, 11/11/10bit "
                 "radix)" << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef pair_digit_extractor<0, 11> d0;
      typedef pair_digit_extractor<1, 11> d1;
      typedef pair_digit_extractor<2, 10, 22> d2;

      d0 di0;
      d1 di1;
      d2 di2;
      crade::detail::multihist_radix_sorter<d0, d1, d2>::sort(data, data + n,
                                                              di0, di1, di2);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout << "std::sort (int)" << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      std::sort(data, data + n);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout << "basic_int_sort" << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      basic_int_sort(data, n);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout << "crade::detail::multihist_radix_sorter (8bit radix)"
              << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef crade::integral_digit_extractor<int, 0> d0;
      typedef crade::integral_digit_extractor<int, 1> d1;
      typedef crade::integral_digit_extractor<int, 2> d2;
      typedef crade::integral_digit_extractor<int, 3> d3;

      d0 di0;
      d1 di1;
      d2 di2;
      d3 di3;
      crade::detail::multihist_radix_sorter<d0, d1, d2, d3>::sort(
          data, data + n, di0, di1, di2, di3);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout << "crade::detail::multihist_radix_sorter (11/11/10 radix)"
              << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef crade::integral_digit_extractor<int, 0, 11> d0;
      typedef crade::integral_digit_extractor<int, 1, 11> d1;
      typedef crade::integral_digit_extractor<int, 2, 10, 22> d2;

      d0 di0;
      d1 di1;
      d2 di2;
      crade::detail::multihist_radix_sorter<d0, d1, d2>::sort(data, data + n,
                                                              di0, di1, di2);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout << "int_sort" << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      int_sort<crade::buffered_bucket_storage<int, 256, 64>>(data, n);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout << "int_sort2" << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      int_sort2<crade::buffered_bucket_storage<int, 256, 64>>(data, n);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout
        << "crade::detail::serial_lsb_radix_sorter::sort (int, 8bit radix)"
        << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef crade::integral_digit_extractor<int, 0> d0;
      typedef crade::integral_digit_extractor<int, 1> d1;
      typedef crade::integral_digit_extractor<int, 2> d2;
      typedef crade::integral_digit_extractor<int, 3> d3;
      typedef crade::buffered_bucket_storage<int, d0::digit_states, 64> storage;

      d0 di0;
      d1 di1;
      d2 di2;
      d3 di3;
      crade::detail::serial_lsb_radix_sorter<storage, d0, d1, d2, d3>::sort(
          data, data + n, di0, di1, di2, di3);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout
        << "crade::detail::serial_lsb_radix_sorter::sort (int, 10/10/12 radix)"
        << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef crade::integral_digit_extractor<int, 0, 10> d0;
      typedef crade::integral_digit_extractor<int, 1, 10> d1;
      typedef crade::integral_digit_extractor<int, 2, 12, 20> d2;
      typedef crade::buffered_bucket_storage<int, d2::digit_states, 64> storage;

      d0 di0;
      d1 di1;
      d2 di2;
      crade::detail::serial_lsb_radix_sorter<storage, d0, d1, d2>::sort(
          data, data + n, di0, di1, di2);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  {
    int *data = new int[n];
    assert(data);

    std::generate_n(data, n, rand);
    std::cout
        << "crade::detail::serial_msb_radix_sorter::sort (int, 8bit radix)"
        << std::endl;
    {
      boost::timer::auto_cpu_timer timer;
      typedef crade::integral_digit_extractor<int, 0> d0;
      typedef crade::integral_digit_extractor<int, 1> d1;
      typedef crade::integral_digit_extractor<int, 2> d2;
      typedef crade::integral_digit_extractor<int, 3> d3;
      typedef crade::buffered_bucket_storage<int, d0::digit_states, 64> storage;

      d0 di0;
      d1 di1;
      d2 di2;
      d3 di3;
      crade::detail::serial_msb_radix_sorter<storage, d3, d2, d1, d0>::sort(
          data, data + n, di3, di2, di1, di0);
    }
    std::cout << std::boolalpha;
    std::cout << std::is_sorted(data, data + n) << std::endl;

    delete[] data;
  }

  return 0;
}
